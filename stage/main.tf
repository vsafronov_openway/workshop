resource "yandex_vpc_network" "internal" {
  name = "internal"
}

resource "yandex_vpc_subnet" "internal-c" {
  name       = "internal-c"
  zone = var.zone
  network_id = yandex_vpc_network.internal.id
  v4_cidr_blocks = ["10.202.0.0/16"]
}