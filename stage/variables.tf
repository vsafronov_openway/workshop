variable "env" {
  default = "stage"
}

variable "folder_id" {
  default = "b1ga5s3k1alml6au8c32"
}

variable "cloud_id" {
  default = "b1gi4rfq1lfkt0a0hts8"
}

variable "zone" {
  default = "ru-central1-c"
}